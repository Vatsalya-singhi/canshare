import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { map, tap, takeWhile } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class SocketService {

    public configuration: any = {};
    public userList$: BehaviorSubject<any[]> = new BehaviorSubject(null);
    public alive: boolean = true;

    constructor(public socket: Socket, public auth: AuthService) {
        // this.auth.user$
        //     .pipe(takeWhile(() => this.alive))
        //     .subscribe(
        //         (data) => {
        //             if (this.auth.user && this.auth.user.id != null) {
        //                 try {
        //                     console.log('socket connect called');
        //                     this.socket.connect();
        //                 } catch (err) {
        //                     console.log('Error occured', err);
        //                 }
        //             }
        //         })
    }

    public ngOnInit(): void {
        // try {
        //     console.log('socket connect called');
        //     this.socket.connect();
        // } catch (err) {
        //     console.log('Error occured', err);
        // }
    }
    public ngOnDestroy(): void {
        this.alive = false;
        this.socket.disconnect();
        this.socket.removeAllListeners();
    }

    /**
    * Emitters
    */
    public sendPoints(action: string, points: any): void {
        this.socket.emit('send-points', {
            action: action,
            points: points,
            room: this.configuration.room,
            authID: this.auth.user.id,
        });
    }

    public specialPoints(points: any, to: any, room: string): void {
        this.socket.emit('special-points', {
            action: "special",
            points: points,
            to: to,
            room: this.configuration.room || room,
            authID: this.auth.user.id,
        });
    }

    // FRESH START
    public createRoom(roomName: string, password: string, editCan: boolean, authID: string, photoURL: string): Promise<any> {
        this.configuration = {
            username: this.auth.user.name,
            room: roomName,
            password: password,
            role: 'creator',
            editCan: editCan,
            authID: authID,
            photoURL: photoURL,
        };
        this.socket.emit('create-room', this.configuration);
        return this.socket.fromOneTimeEvent('create-room-callback');
    }

    public joinRoom(roomName: string, password: string, userName: string, role: string = 'joinee', authID: string, photoURL: string): Promise<any> {
        this.configuration = {
            username: (userName && userName.length > 0) ? userName : (this.auth.user.name ? this.auth.user.name : "New User"),
            room: roomName,
            password: password,
            role: role,
            authID: authID,
            photoURL: photoURL
        };
        this.socket.emit('join-room', this.configuration);
        return this.socket.fromOneTimeEvent('join-room-callback');
    }

    public leaveRoom() {
        this.socket.emit('leave-room')
        localStorage.removeItem('room');
        this.configuration = {};
    }

    public updateConfig(roomName: string, id: string, editCan: boolean) { // new editCan
        let payload = {
            roomName,
            id,
            editCan,
        };
        this.socket.emit('update-config', payload);
        return this.socket.fromOneTimeEvent('update-config-callback');
    }
    /**
    * Event Listeners
    */

    public fetchMessage(): Observable<any> {
        return this.socket.fromEvent('message');
    }

    public roomUsers(): Observable<any> {
        return this.socket.fromEvent('room-users');
    }

    public fetchPoints(): Observable<any> {
        return this.socket.fromEvent('fetch-points');
    }

    public specialCall(): Observable<any> {
        return this.socket.fromEvent('specialCall');
    }

    // have to use this
    public config(): Observable<any> {
        return this.socket.fromEvent('config');
    }

    public errorCases(): Observable<any> {
        return this.socket.fromEvent('error').pipe(
            tap(str => {
                if (str == "Room with similar name already exist..") {
                    this.configuration = {};
                }
                if (str == "No room exists") {
                    this.configuration = {};
                }
                if (str == "User already joined") {

                }
                console.log('str=>', str);
            })
        );
    }


    // public setUpSocket() {
    //     if (this.socket.ioSocket.disconnected) {
    //         console.log('Disconnected trying to connect back');
    //         this.socket.connect();
    //     }
    // }


    /**
    * Video Functions
    */

    public sendBroadcast() {
        return this.socket.emit("broadcast", {
            room: this.configuration.room
        });
    }

    public sendWatcher(id: string) {
        return this.socket.emit("watcher", {
            id: id
        });
    }

    public sendCandidate(id: any, candidate: RTCIceCandidate) {
        return this.socket.emit('candidate', {
            id: id,
            candidate: candidate,
            // candidate : encodeURIComponent(JSON.stringify(candidate))
        })
    }

    public sendOffer(id: any, offer: RTCSessionDescription) {
        return this.socket.emit("offer", {
            id: id,
            offer: offer,
            // localDescription : localDescription,
            // localDescription : encodeURIComponent(JSON.stringify(localDescription))
        });
    }

    public sendAnswer(id: any, answer: any) {
        return this.socket.emit("answer", {
            id: id,
            answer: answer,
            // localDescription : encodeURIComponent(JSON.stringify(localDescription)),
        })
    }

    /**
    * Video Event Listeners
    */

    public broadcastListener(): Observable<any> {
        return this.socket.fromEvent('broadcast');
    }

    public watchListener(): Observable<any> {
        return this.socket.fromEvent('watcher');
    }

    public offerListen(): Observable<any> {
        return this.socket.fromEvent("offer");
    }

    public answerListen(): Observable<any> {
        return this.socket.fromEvent("answer");
    }

    public candidateListen(): Observable<any> {
        return this.socket.fromEvent("candidate");
    }

    public disconnectPeer(): Observable<any> {
        return this.socket.fromEvent("disconnectPeer");
    }
}