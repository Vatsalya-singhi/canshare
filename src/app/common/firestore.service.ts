import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';

import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class FirestoreService {

    public classes: any = {};

    constructor(
        private db: AngularFirestore,
        private storage: AngularFireStorage,
    ) {

    }

    /**************************************************************
     ****************Collection Functions***************************
     *************************************************************/

    public newId(): string {
        return this.db.createId();
    }

    public queryDocWithMatchingField(className: string, collectionName: string, fieldName: string, condition: any, value: any): Observable<any> {
        console.log('check this ! =>', className, collectionName, fieldName, condition, value);
        return this.db.collection(collectionName, ref => ref.where(fieldName, condition, value)).valueChanges()
            .pipe(
                map((data: any) => {
                    console.log('data=>', data);
                    return data.map(d => new this.classes[className](d));
                })
                , shareReplay(1));
    }

    // public updateDocument(collectionName: string, documentId: string, payload: any): Promise<void> {
    //     return this.db.collection(collectionName)
    //         .doc(documentId)
    //         .set({ ...payload }, { merge: true });
    // }

    // public deleteDocument(collectionName: string, id: string): Promise<void> {
    //     return this.db.collection(collectionName).doc(id).delete();
    // }

    public getAllDocuments(className: string, collectionName: string): Observable<any> {
        return this.db.collection(collectionName).valueChanges()
            .pipe(
                map((data: any) => {
                    console.log('data=>', data);
                    return data.map(d => new this.classes[className](d));
                })
                , shareReplay(1));
    }

    public fetchDocumentInCollection(collectionName: string, id: string): Observable<any> {
        return this.db.collection(collectionName).doc(id).valueChanges();
    }


    // !!!! NEW UPDATED FUNCTIONS !!!!
    public fetchAllDocFromCollection(className: string, collectionName: string): Observable<any[]> {
        return this.db.collection(collectionName).valueChanges()
            .pipe(
                map((data: any) => {
                    return data.map((d: any) => {
                        if (className.length > 0) {
                            return new this.classes[className](d);
                        } else {
                            return d;
                        }
                    });
                })
                , shareReplay(1)
            )
    }

    public fetchDocByIdFromCollection(collectionName: string, documentId: string): Observable<any> {
        return this.db.collection(collectionName).doc(documentId)
            .valueChanges();
    }

    public queryDocWith2MatchingFields(className: string, collectionName: string, fieldName1: string, condition1: any, value1: any, fieldName2: string, condition2: any, value2: any): Observable<any[]> {
        return this.db.collection(collectionName, ref => ref.where(fieldName1, condition1, value1).where(fieldName2, condition2, value2)).valueChanges()
            .pipe(
                map((data: any) => {
                    return data.map(d => new this.classes[className](d));
                })
                , shareReplay(1)
            );
    }

    public queryDocWith3MatchingFields(collectionName: string, fieldName1: string, condition1: any, value1: string, fieldName2: string, condition2: any, value2: string, fieldName3: string, condition3: any, value3: string): Observable<any[]> {
        return this.db.collection(collectionName, ref => ref.where(fieldName1, condition1, value1).where(fieldName2, condition2, value2).where(fieldName3, condition3, value3))
            .valueChanges();
    }

    public updateDocument(collectionName: string, documentId: string, payload: any): Promise<void> {
        return this.db.collection(collectionName)
            .doc(documentId)
            .set({ ...payload }, { merge: true });
    }

    public deleteDocument(collectionName: string, id: string): Promise<void> {
        return this.db.collection(collectionName).doc(id).delete();
    }

    /**************************************************************
     ****************Subcollection Functions*********************************
     *************************************************************/

    public fetchSubCollection(className: string, collectionName: string, documentId: string, subCollectionName: string): Observable<any> {
        return this.db.collection(collectionName).doc(documentId).collection(subCollectionName).valueChanges()
            .pipe(
                map((data: any) => {
                    return data.map(d => new this.classes[className](d));
                })
                , shareReplay(1)
            );
    }

    public fetchSubCollectionByLimit(className: string, collectionName: string, documentId: string, subCollectionName: string, limit: number): Observable<any> {
        return this.db.collection(collectionName).doc(documentId).collection(subCollectionName, ref => {
            return ref.orderBy('createdAt', 'desc')
                .limit(limit)
        }).valueChanges()
            .pipe(
                map((data: any) => {
                    return data.map(d => new this.classes[className](d));
                })
                , shareReplay(1)
            );
    }

    public fetchDocumentInSubCollection(className: string, collectionName: string, documentId: string, subCollectionName: string, documentId2: string): Observable<any> {
        return this.db.collection(collectionName).doc(documentId).collection(subCollectionName).doc(documentId2).valueChanges()
            .pipe(
                map((data: any) => {
                    return new this.classes[className](data);
                })
                , shareReplay(1)
            );
    }

    public querysubcollectionMatchingField(collectionName: string, documentId: string, subCollectionName: string, fieldName1: string, condition1: any, value1: string): Observable<any> {
        return this.db.collection(collectionName).doc(documentId).collection(subCollectionName, ref => ref.where(fieldName1, condition1, value1)).valueChanges();
    }

    public querysubcollection2MatchingField(collectionName: string, documentId: string, subCollectionName: string, fieldName1: string, condition1: any, value1: string, fieldName2: string, condition2: any, value2: string): Observable<any> {
        return this.db.collection(collectionName).doc(documentId).collection(subCollectionName, ref => ref.where(fieldName1, condition1, value1).where(fieldName2, condition2, value2)).valueChanges();
    }

    public querysubcollection3MatchingField(collectionName: string, documentId: string, subCollectionName: string, fieldName1: string, condition1: any, value1: string, fieldName2: string, condition2: any, value2: string, fieldName3: string, condition3: any, value3: string): Observable<any> {
        return this.db.collection(collectionName).doc(documentId).collection(subCollectionName, ref => ref.where(fieldName1, condition1, value1).where(fieldName2, condition2, value2).where(fieldName3, condition3, value3)).valueChanges();
    }

    public updateSubCollection(collectionName: string, documentId: string, subCollectionName: string, documentId2: string, payload: any): Promise<void> {

        return this.db.collection(collectionName)
            .doc(documentId)
            .collection(subCollectionName)
            .doc(documentId2)
            .set({ ...payload }, { merge: true });
    }

    public deleteDocumentInSubCollection(collectionName: string, subCollectionName: string, documentId: string, documentId2: string): Promise<void> {
        return this.db.collection(collectionName)
            .doc(documentId)
            .collection(subCollectionName)
            .doc(documentId2)
            .delete();
    }

    /**************************************************************
     ****************GrandSubCollection Functions*********************************
     *************************************************************/

    public fetchGrandSubCollection(className: string, collectionName: string, subCollectionName: string, grandSubCollectionName: string, documentId: string, documentId2: string): Observable<any> {
        return this.db.collection(collectionName).doc(documentId).collection(subCollectionName).doc(documentId2).collection(grandSubCollectionName).valueChanges()
            .pipe(
                map((data: any) => {
                    return data.map(d => new this.classes[className](d));
                })
                , shareReplay(1)
            );
    }

    public fetchDocumentInGrandSubCollection(className: string, collectionName: string, subCollectionName: string, grandSubCollectionName: string, documentId: string, documentId2: string, documentId3: string): Observable<any> {
        return this.db.collection(collectionName).doc(documentId).collection(subCollectionName).doc(documentId2).collection(grandSubCollectionName).doc(documentId3).valueChanges()
            .pipe(
                map((data: any) => {
                    if (!data) {
                        return null;
                    }
                    return new this.classes[className](data);
                })
                , shareReplay(1)
            );
    }

    public queryGrandSubcollectionMatchingField(collectionName: string, subCollectionName: string, grandSubCollectionName: string, documentId: string, documentId2: string, fieldName1: string, condition1: any, value1: string): Observable<any> {
        return this.db.collection(collectionName).doc(documentId).collection(subCollectionName).doc(documentId2).collection(grandSubCollectionName, ref => ref.where(fieldName1, condition1, value1)).valueChanges();
    }

    public queryGrandSubcollection2MatchingField(collectionName: string, subCollectionName: string, grandSubCollectionName: string, documentId: string, documentId2: string, fieldName1: string, condition1: any, value1: string, fieldName2: string, condition2: any, value2: string): Observable<any> {
        return this.db.collection(collectionName).doc(documentId).collection(subCollectionName).doc(documentId2).collection(grandSubCollectionName, ref => ref.where(fieldName1, condition1, value1).where(fieldName2, condition2, value2)).valueChanges();
    }

    public queryGrandSubcollection3MatchingField(collectionName: string, subCollectionName: string, grandSubCollectionName: string, documentId: string, documentId2: string, fieldName1: string, condition1: any, value1: string, fieldName2: string, condition2: any, value2: string, fieldName3: string, condition3: any, value3: string): Observable<any> {
        return this.db.collection(collectionName).doc(documentId).collection(subCollectionName).doc(documentId2).collection(grandSubCollectionName, ref => ref.where(fieldName1, condition1, value1).where(fieldName2, condition2, value2).where(fieldName3, condition3, value3)).valueChanges();
    }

    public updateGrandSubCollection(collectionName: string, subCollectionName: string, grandSubCollectionName: string, documentId: string, documentId2: string, documentId3: string, payload: any): Promise<void> {
        return this.db.collection(collectionName)
            .doc(documentId)
            .collection(subCollectionName)
            .doc(documentId2)
            .collection(grandSubCollectionName)
            .doc(documentId3)
            .set(payload, { merge: true });
    }

    public deleteGrandSubCollection(collectionName: string, subCollectionName: string, grandSubCollectionName: string, documentId: string, documentId2: string, documentId3: string): Promise<void> {
        return this.db.collection(collectionName)
            .doc(documentId)
            .collection(subCollectionName)
            .doc(documentId2)
            .collection(grandSubCollectionName)
            .doc(documentId3)
            .delete();
    }

    public getNewId(): string {
        return this.db.createId();
    }

}
