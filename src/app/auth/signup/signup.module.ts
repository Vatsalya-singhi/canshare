import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SignupPageRoutingModule } from './signup-routing.module';

import { SignupPage } from './signup.page';
import { AuthService } from '../auth.service';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';

import { Ng2ImgMaxModule } from 'ng2-img-max';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SignupPageRoutingModule,
        ReactiveFormsModule,
        AngularFireAnalyticsModule,
        Ng2ImgMaxModule,
    ],
    declarations: [
        SignupPage,
    ],
    providers: [
        AuthService
    ]
})
export class SignupPageModule { }
