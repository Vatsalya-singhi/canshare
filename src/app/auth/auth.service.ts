import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAnalytics } from '@angular/fire/analytics';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';

import * as firebase from 'firebase';
// import { firebase } from '@firebase/app';

import { Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { FirestoreService } from '../common/firestore.service';
import { User } from '../common/model/user.model';

import { auth } from 'firebase/app';

import { Platform } from '@ionic/angular';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    public user$: Observable<User>;
    public user: User = null;

    constructor(
        public afAuth: AngularFireAuth,
        private db: AngularFirestore,
        public router: Router,
        public route: ActivatedRoute,
        public storage: AngularFireStorage,
        public firestore: FirestoreService,
        public analytics: AngularFireAnalytics,
        public plt: Platform,
        public gplus: GooglePlus
    ) {
        this.user$ = this.afAuth.authState.pipe(
            switchMap(user => {
                if (user) {
                    console.log('user=>', user);
                    return this.db.doc<User>(`User/${user.uid}`).valueChanges()
                } else {
                    return of(null);
                }
            }),
            tap(async (userObject: User) => {
                console.log('userObject=>', userObject);
                this.user = userObject;
                await this.analytics.logEvent('login');
            })
        )


    }


    /**************************************************************
     ****************Public Functions*****************************
    *************************************************************/

    public async googleSignin(): Promise<any> {
        console.log('check pltform=> ', this.plt.is('mobileweb'), this.plt.is('mobile'));
        console.log('check pltform=> ', this.plt.is('cordova'), this.plt.is('ios'), this.plt.is('android'));

        if (this.plt.is('cordova') && (this.plt.is('ios') || this.plt.is('android'))) {
            console.log('1 is called');
            // run native code
            await this.plt.ready();
            console.log('platform is now ready');

            let gplusUser = await this.gplus.login({
                'webClientId': '914595712966-51qanfnpqthcd5a2m7ivv30rmtbg3ei6.apps.googleusercontent.com',
                'offline': true,
                // 'scopes': 'profile email',
            })

            return this.afAuth.signInWithCredential(
                firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken)
            ).then((signInRes: any) => {
                const credentials = signInRes;
                console.log('credentials=>', credentials.user);
                this.updateUserData(credentials.user);
                return credentials.user;
            }).catch((err) => {
                console.log('err=>', err);
                return err;
            })
        }

        // default case
        let provider = new firebase.auth.GoogleAuthProvider();
        provider.setCustomParameters({
            prompt: 'select_account'
        });

        if (this.plt.is('mobileweb') || this.plt.is('mobile')) {
            console.log('2 is called');
            localStorage.setItem('google2method',"true");
            return this.afAuth.signInWithRedirect(provider);
        }

        console.log('3 is called');
        return this.afAuth.signInWithPopup(provider).then((signInRes) => {
            const credentials = signInRes;
            console.log('credentials=>', credentials.user);
            this.updateUserData(credentials.user);
            return credentials.user;
        }).catch((err) => {
            console.log('err=>', err);
            return err;
        });

    }

    public signInWithEmail(credentials: any): Promise<firebase.User> {
        console.log('Sign in with email');
        return this.afAuth.signInWithEmailAndPassword(credentials.email,
            credentials.password).then((credentials: auth.UserCredential) => {
                console.log('credentials=>', credentials.user);
                // this.updateUserData(credentials.user);
                return credentials.user;
            });
    }

    public registerWithEmail(credentials: any): Promise<void> {
        console.log('Register with email');

        return this.afAuth.createUserWithEmailAndPassword(credentials.email, credentials.password).then((credentials: auth.UserCredential) => {
            this.updateUserData(credentials.user);
        });

        // OLD WAY
        // return firebase.auth().createUserWithEmailAndPassword(credentials.email, credentials.password).then((credentials: auth.UserCredential) => {
        //     this.updateUserData(credentials.user);
        // });
    }

    public updatePassword(credentials: any): Promise<void> {
        console.log('Register with email');
        console.log('curr user =>', firebase.auth().currentUser);
        return firebase.auth()
            .currentUser.updatePassword(credentials.password);
    }

    public async signOut(): Promise<any> {
        localStorage.clear();
        await this.afAuth.signOut();
        if (this.plt.is('cordova') && (this.plt.is('ios') || this.plt.is('android'))) {
            await this.gplus.logout();
        }

        this.router.navigate(['/login']);

        // setTimeout(() => {
        //     window.location.href = "#";
        // }, 1000);

    }

    public checkAuthorisation(collectionName: string, email: string): Observable<any> {
        return this.firestore.queryDocWithMatchingField('User', `${collectionName}`, 'email', '==', email);
    }

    private updateUserData(credentials: any): void {
        if (credentials == null || !credentials) {
            console.log('credentials is null thus returning');
            return;
        }
        const userRef: AngularFirestoreDocument<any> = this.db.doc(`User/${credentials.uid}`);
        const data = new User({
            id: credentials.uid,
            email: credentials.email,
            name: credentials.displayName,
            createdBy: credentials.uid,
            updatedBy: credentials.uid,
            photoURL: credentials.photoURL,
        });

        console.log('data=>', data);
        this.user = new User(data);
        userRef.set({
            ...data
        }, { merge: true });
    }

}